
# base image
FROM node:12.2.0-alpine

# set working directory
WORKDIR ./

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
ADD package.json /package.json

# install and cache app dependencies
COPY package.json /usr/src/app/package.json
RUN yarn
COPY . . 
EXPOSE 3000:3000

# start app
CMD ["yarn", "start"]